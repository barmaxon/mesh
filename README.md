# Test project for Meshgroup

# Installation

  - Make .env from.env.example (change external port if 99 is busy)
  - Build docker services
  - Run init commands

```sh
# go to project directory 'cd /path/to/project'
cp .env.example .env
docker-compose up -d
docker exec -it hotger_web composer install
docker exec -it hotger_web php artisan migrate
npm i && npm run prod
```

If external port is 99:
Open browser and go to http://locahost:99
