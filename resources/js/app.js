require('./bootstrap');

window.Vue = require('vue');
Vue.component('upload-component', require('./components/Upload.vue').default);
Vue.component('list-component', require('./components/List.vue').default);
const app = new Vue({
    el: '#app',
});
