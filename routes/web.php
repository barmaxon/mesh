<?php

use App\Http\Controllers\MainController;
use Illuminate\Support\Facades\Broadcast;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[MainController::class, 'index']);
Route::get('/list',[MainController::class, 'list']);
Route::get('/uploads',[MainController::class, 'get_uploads']);
Route::get('/rows',[MainController::class, 'get_rows']);

Route::post('/upload', [MainController::class, 'upload']);
