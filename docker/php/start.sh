set -e

php /var/www/html/artisan rabbitmq:consume --verbose --tries=3 --timeout=90

exec "$@"
