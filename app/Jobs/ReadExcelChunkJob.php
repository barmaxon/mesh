<?php

namespace App\Jobs;

use App\Classes\CustomExcel;
use App\Events\UploadProgressEvent;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\UploadedFile;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Reader\IReader;

class ReadExcelChunkJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    /**
     * @var int
     */
    private $importID;
    /**
     * @var array
     */
    private $data;

    public function __construct(array $data, int $importID)
    {
        $this->importID = $importID;
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function handle()
    {
        $redisImportsKey = "import_progress:$this->importID";
        $redis = Redis::connection();
        $progress = explode(',', $redis->get($redisImportsKey));
        $count = count($this->data);
        Log::info("jc: $count");

        if($progress) {
            $new = (int)$progress[0]+$count;
            $newValue = "$new,$progress[1]";
        }else {
            $newValue = $count;
        }

        DB::table('rows')->insert($this->data);
        $redis->set($redisImportsKey, $newValue);
        event(new UploadProgressEvent($newValue));
    }
}
