<?php
namespace App\Classes;

use App\Jobs\ReadExcelChunkJob;
use Carbon\Carbon;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redis;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\IReader;

class CustomExcel
{

    /** @var  UploadedFile $excel */
    /** @var  ChunkReadFilter $chunkFilter */
    /** @var  IReader $reader */
    private $excel;
    private $chunkFilter;
    private $reader;
    private $count;

    const COLS = [
        1 => 'id',
        2 => 'name',
        3 => 'date'
    ];

    /**
     * CustomExcel constructor.
     * @param UploadedFile $excel
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function __construct(UploadedFile $excel)
    {
        $this->excel = $excel;

        $inputFileType = ucfirst($excel->getClientOriginalExtension());
        $reader =IOFactory::createReader($inputFileType);
        $chunkFilter = new ChunkReadFilter();
        $reader->setReadFilter($chunkFilter);
        $this->chunkFilter = $chunkFilter;
        $this->reader = $reader;

    }

    /**
     * @return int
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function countRows() {

        $this->chunkFilter->setRows(1,65536);
        $spr = $this->reader->load($this->excel)->getActiveSheet();

        // don't count headers
        $count = -1;
        $hCol = Coordinate::columnIndexFromString($spr->getHighestColumn());
        for ($row = 1; $row <= $spr->getHighestRow(); ++$row) {
            $tmp = [];
            for ($col = 1; $col <= $hCol; ++$col) {
                $val = $spr->getCellByColumnAndRow($col, $row)->getCalculatedValue();
                if($val) {
                    $tmp[] = $val;
                }
            }
            if($tmp) {
                $count++;
            }
        }
        $this->count = $count;
        return $count;
    }


    /**
     * @param int $chunkSize
     * @param $importID
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public function importToDB($importID, $chunkSize = 1000) {
        $this->chunkFilter->setRows(1, 65536);

        if(!$this->count) {
            $this->countRows();
        }

        $idRegexp = '/=A(\d+)\+1/i';

        $redis = Redis::connection();
        $redis->set("import_progress:$importID", "0,$this->count");

        for ($startRow = 2; $startRow <= $this->count; $startRow += $chunkSize) {
            Log::info("$startRow");
            $spreadsheet = $this->reader->load($this->excel);
            $activeSheet = $spreadsheet->getActiveSheet();

            $hRow = $startRow + $chunkSize;
            $hCol = Coordinate::columnIndexFromString($activeSheet->getHighestColumn());
            $data = [];
            $testData = [];
            for ($row = $startRow; $row < $hRow; ++$row) {

                $tmp = [];
                $test = [];
                for ($col = 1; $col <= $hCol; ++$col) {
                    $val = $activeSheet->getCellByColumnAndRow($col, $row)->getCalculatedValue();
                    $colName = CustomExcel::COLS[$col];
                    $test[$colName] = $val;

                    if($val) {
                        if($colName === 'date') {
                            $unix = ($val - 25569) * 86400;
                            $val = gmdate("Y-m-d", $unix);
                        }

                        if($colName === 'id' && preg_match($idRegexp, $val, $m)) {
                            $val = "$m[1]";
                        }

                        $tmp[$colName] = "$val";
                    }
                }

                $testData[] = $test;
                if($tmp) {
                    $tmp['import_id'] = "$importID";
                    $data[] = $tmp;
                }
            }
            $cData = count($data);
            Log::info('count:'.$cData);
//            Log::info('one:'.print_r($data[0], true));
//            Log::info(print_r(array_slice($testData, 20, 10), true));

            dispatch(new ReadExcelChunkJob($data, $importID));
        }
    }


}
