<?php

namespace App\Http\Controllers;

use App\Classes\CustomExcel;
use App\Models\Import;
use App\Models\Row;
use Carbon\Carbon;
use Illuminate\Http\Request;

class MainController extends Controller
{

    public function list() {
        return view('list');
    }

    public function index()
    {
        return view('main');
    }

    /**
     * @param Request $request
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function upload(Request $request) {
        $file = $request->file('file');

        $import = new Import(['name' => $file->getClientOriginalName()]);
        $import->save();

        $ce = new CustomExcel($file);
        $ce->importToDB($import->id);
    }

    public function get_uploads() {
        $imports = Import::query()->get()->toArray();
        foreach ($imports as $key => $import) {
            $imports[$key]['created_at'] = Carbon::parse($import['created_at'])->format('d.m.y H:i');
        }
        return response()->json($imports);
    }

    public function get_rows(Request $request) {
        $id = $request->get('id');
        return response()->json(Row::query()->where('import_id', $id)->get());
    }
}
